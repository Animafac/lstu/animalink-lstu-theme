# Animalink Lstu theme

Lstu theme (based on the default theme) used on [Animalink](https://link.animafac.net/)

## Install

This theme needs to be installed in the `themes/` folder of Lstu.

You then need to edit `lstu.conf` to enable it:

```perl
theme => 'animalink-lstu-theme',
```
